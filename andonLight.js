const Gpio = require('pigpio').Gpio

const env = require('./env')
const kafkaHelper = require('./util/kafkaHelper')

const DEBUG = false
const colors = ['blue', 'green', 'orange', 'red']

const lampInfo = {
  blue: {
    gpioPin: 26,
    relay: 'J5'
  },

  green: {
    gpioPin: 6,
    relay: 'J4'
  },

  orange: {
    gpioPin: 22,
    relay: 'J3'
  },

  red: {
    gpioPin: 4,
    relay: 'J2'
  }
}

function init () {
  // create GPIO ports
  colors.forEach(color => {
    const info = lampInfo[color]
    info.port = new Gpio(info.gpioPin, { mode: Gpio.OUTPUT })
  })

  // turn all the lights off
  processor({ all: 'off' })
}

async function watcher () {
  var client = kafkaHelper.createClient()
  const consumer = await kafkaHelper.createConsumerForLatest(client, env.andonLampTopic)

  consumer.onMessage(data => {
    var message = deserializer(data.value)
    if (message) { processor(message) }
  })
}

function deserializer (text) {
  text = (text || '')
  if (!text.length) { return undefined }

  if (text[0] === '{' && text[text.length - 1] === '}') {
    try {
      return JSON.parse(text)
    } catch (err) {
      return undefined
    }
  }

  let result;
  ['all'].concat(colors).forEach(property => {
    if (text.toLowerCase().indexOf(property) === 0) {
      result = {}
      result[property] = text.substr(property.length)
    }
  })

  return result
}

/*
    {
        <color-name>: command
    }

    where command is 'on', 'off', or 'blink [on milliseconds] [, off milliseconds]'
    If off milliseconds is omitted, lamp will blink with 50% duty cycle.
    If both millisecond paramters are omitted, the default is 1000 1000

    The value of 'all' can be used for color name.
*/
function processor (data) {
  if (data.all) {
    const parsedCommand = parser(null, data.all)
    if (parsedCommand) {
      colors.forEach(color => {
        dispatcher(Object.assign({}, parsedCommand, { color }))
      })
    }
  }

  colors.forEach(color => {
    const command = data[color]
    if (command) {
      const parsedCommand = parser(color, command)
      if (parsedCommand) { dispatcher(parsedCommand) }
    }
  })
}

function dispatcher (parsedCommand) {
  switch (parsedCommand.command) {
    case 'on':
      executeOn(parsedCommand)
      break

    case 'off':
      executeOff(parsedCommand)
      break

    case 'blink':
      executeBlink(parsedCommand)
      break
  }
}

function executeOff (parsedCommand) {
  const color = parsedCommand.color
  killTimer(color)

  if (DEBUG) { console.log(`${color} lamp is off`) } else { lampInfo[color].port.digitalWrite(0) }
}

function executeOn (parsedCommand) {
  const color = parsedCommand.color
  killTimer(color)

  if (DEBUG) { console.log(`${color} lamp is on`) } else { lampInfo[color].port.digitalWrite(1) }
}

function executeBlink (parsedCommand) {
  const color = parsedCommand.color
  killTimer(color)

  const dutyOn = init => {
    if (Object.prototype.hasOwnProperty.call(lampInfo[color], 'timer') || init) {
      executeOn({ color })
      lampInfo[color].timer = setTimeout(() => { dutyOff() }, parsedCommand.dutyOn)
    }
  }

  const dutyOff = () => {
    if (Object.prototype.hasOwnProperty.call(lampInfo[color], 'timer')) {
      executeOff({ color })
      lampInfo[color].timer = setTimeout(() => { dutyOn() }, parsedCommand.dutyOff)
    }
  }

  dutyOn(true)
}

function killTimer (color) {
  if (lampInfo[color].timer) {
    clearTimeout(lampInfo[color].timer)
    delete (lampInfo[color].timer)
  }
}

function parser (color, command) {
  var text = command.toLowerCase()

  if (text.indexOf('on') !== -1) { return { color, command: 'on' } }

  if (text.indexOf('off') !== -1) { return { color, command: 'off' } }

  var index = text.indexOf('blink')
  if (index === -1) { return undefined }

  var dutyOn = 1000
  var dutyOff = 1000
  text = command.substr(index + 5)
  if (text.length) {
    const parts = text.split(',')
    dutyOn = parts.length ? Number(parts[0]) : 1000
    dutyOff = parts.length > 1 ? Number(parts[1]) : dutyOn
  }

  return { color, command: 'blink', dutyOn, dutyOff }
}

module.exports = {
  init,
  watcher,
  processor,
  parser,
  dispatcher
}
