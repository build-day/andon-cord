const Gpio = require('pigpio').Gpio

const J2 = new Gpio(4, { mode: Gpio.OUTPUT })
const J3 = new Gpio(22, { mode: Gpio.OUTPUT })
const J4 = new Gpio(6, { mode: Gpio.OUTPUT })
const J5 = new Gpio(26, { mode: Gpio.OUTPUT })

J2.digitalWrite(0)
J3.digitalWrite(0)
J4.digitalWrite(0)
J5.digitalWrite(0)

const ledState = {
  J2: 0,
  J3: 0,
  J4: 0,
  J5: 0
}

setInterval(() => {
  ledState.J2 = ledState.J2 ? 0 : 1
  console.log('J2', ledState.J2)
  J2.digitalWrite(ledState.J2)
}, 100)
setInterval(() => {
  ledState.J3 = ledState.J3 ? 0 : 1
  console.log('J3', ledState.J3)
  J3.digitalWrite(ledState.J3)
}, 130)
setInterval(() => {
  ledState.J4 = ledState.J4 ? 0 : 1
  console.log('J4', ledState.J4)
  J4.digitalWrite(ledState.J4)
}, 160)
setInterval(() => {
  ledState.J5 = ledState.J5 ? 0 : 1
  console.log('J5', ledState.J5)
  J5.digitalWrite(ledState.J5)
}, 200)
