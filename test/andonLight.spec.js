const expect = require('chai').expect

const andonLight = require('../andonLight')

describe('andonLight tests', () => {
  // Dispatcher tests
  xit('dispatcher toggle', () => {
    andonLight.init()
    andonLight.dispatcher({ color: 'green', command: 'on' })
    andonLight.dispatcher({ color: 'green', command: 'off' })
  })

  xit('dispatcher blink', done => {
    andonLight.init()
    andonLight.dispatcher({
      color: 'orange',
      command: 'blink',
      dutyOn: 2000,
      dutyOff: 1000
    })

    setTimeout(() => {
      andonLight.dispatcher({ color: 'orange', command: 'off' })
      done()
    }, 10000)
  })

  // Parser tests
  it('parser: on', () => {
    const result = andonLight.parser('blue', 'on')

    expect(result.color).to.be.eq('blue')
    expect(result.command).to.be.eq('on')
  })

  xit('parser: off', () => {
    const result = andonLight.parser('green', 'off')

    expect(result.color).to.be.eq('green')
    expect(result.command).to.be.eq('off')
  })

  xit('parser: blink', () => {
    var result = andonLight.parser('orange', 'blink')
    expect(result.color).to.be.eq('orange')
    expect(result.command).to.be.eq('blink')
    expect(result.dutyOn).to.be.eq(1000)
    expect(result.dutyOff).to.be.eq(1000)

    result = andonLight.parser('orange', 'blink 123')
    expect(result.color).to.be.eq('orange')
    expect(result.command).to.be.eq('blink')
    expect(result.dutyOn).to.be.eq(123)
    expect(result.dutyOff).to.be.eq(123)

    result = andonLight.parser('orange', 'blink 123, 456')
    expect(result.color).to.be.eq('orange')
    expect(result.command).to.be.eq('blink')
    expect(result.dutyOn).to.be.eq(123)
    expect(result.dutyOff).to.be.eq(456)
  })
})
