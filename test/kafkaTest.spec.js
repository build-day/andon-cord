const assert = require('chai').assert
const expect = require('chai').expect

const config = require('../env')
const kafka = require('kafka-node')
const kafkaHelper = require('../util/kafkaHelper')

describe('Kafka Tests', () => {
  xit('Basic producer/consumer test', done => {
    const PAYLOAD = { a: 0, b: 1, c: 'two' }
    const TOPIC = 'color'

    var client = new kafka.KafkaClient({
      autoConnect: true,
      kafkaHost: config.servers,
      sasl: {
        mechanism: 'plain',
        username: config.username,
        password: config.password
      },
      sslOptions: { rejectUnauthorized: false }
    })

    var consumer = new kafka.Consumer(client, [{
      topic: TOPIC,
      partition: 0,
      offset: 0
    }])

    var producer = new kafka.Producer(client)

    consumer.on('message', data => {
      var message = JSON.parse(data.value)
      expect(message.a).to.be.eq(0)
      expect(message.b).to.be.eq(1)
      expect(message.c).to.be.eq('two')

      consumer.close()
      producer.close()
      client.close()
      done()
    })

    producer.on('ready', () => {
      producer.send([{
        topic: TOPIC,
        partition: 0,
        messages: JSON.stringify(PAYLOAD)
      }], () => assert.fail)
    })

    producer.connect()
  })

  xit('Helper test', done => {
    const PAYLOAD = { a: 0, b: 1, c: 'two' }
    const TOPIC = 'andon-test'

    var client = kafkaHelper.createClient()

    kafkaHelper.createConnectedProducer(client).then(producer => {
      var consumer = kafkaHelper.createConsumer(client, TOPIC)
      consumer.on('message', data => {
        var message = JSON.parse(data.value)
        expect(message.a).to.be.eq(0)
        expect(message.b).to.be.eq(1)
        expect(message.c).to.be.eq('two')

        consumer.close()
        producer.close()
        client.close()
        done()
      })

      kafkaHelper.sendMessage(producer, TOPIC, PAYLOAD)
    })
  })
})
