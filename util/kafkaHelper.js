const env = require('../env')
const kafka = require('kafka-node')

function consumerOnMessage (handler) {
  this.on('message', data => {
    handler(data)
  })

  this.on('offsetOutOfRange', error => {
    console.log(`kafka offset error: ${error}`)
  })

  this.on('error', error => {
    console.log(`kafka error: ${error}`)
  })
}

function createClient (config) {
  const clientConfig = Object.assign({
    autoConnect: true,
    kafkaHost: env.servers,
    sasl: {
      mechanism: 'plain',
      username: env.username,
      password: env.password
    },
    sslOptions: { rejectUnauthorized: false }
  }, config)

  return new kafka.KafkaClient(clientConfig)
}

function createConsumer (client, topic, config) {
  const consumerConfig = Object.assign({
    topic,
    offset: 0,
    partition: 0
  }, config)

  const consumer = new kafka.Consumer(client, [consumerConfig], { fromOffset: 'latest' })
  consumer.onMessage = consumerOnMessage.bind(consumer)
  return consumer
}

async function createConsumerForLatest (client, topic, config) {
  const consumer = createConsumer(client, topic, config)
  consumer.$latestOffset = await fetchLatestOffset(client, topic)
  consumer.setOffset(topic, 0, consumer.$latestOffset - 1)

  consumer.onMessage = consumerOnMessage.bind(consumer)
  return consumer
}

function createConnectedProducer (client) {
  return new Promise(resolve => {
    const producer = new kafka.Producer(client)

    producer.on('ready', status => {
      resolve(producer)
    })

    producer.connect()
  })
}

function fetchLatestOffset (client, topic) {
  return new Promise((resolve, reject) => {
    const offset = new kafka.Offset(client)
    offset.fetchLatestOffsets([topic], (error, offsets) => {
      if (error) {
        reject(new Error(`error fetching latest offsets ${error}`))
      } else {
        const offset = Object.keys(offsets[topic]).reduce((acc, cur) => {
          return Math.max(acc, offsets[topic][cur])
        }, 1)
        resolve(offset)
      }
    })
  })
}

function fetchOffsets (client, topic) {
  return new Promise((resolve, reject) => {
    const offset = new kafka.Offset(client)
    offset.fetch([{
      maxNum: 1,
      partition: 0,
      time: Date.now(),
      topic
    }], (error, data) => {
      if (error) {
        reject(error)
      } else {
        resolve(data)
      }
    })
  })
}

function sendMessage (producer, topic, payload, config) {
  const messageInfo = Object.assign({
    topic,
    partition: 0,
    messages: JSON.stringify(payload)
  }, config)

  producer.send([messageInfo], err => console.error(`sendMessage failed: ${err}`))
}

module.exports = {
  createClient,
  createConnectedProducer,
  createConsumer,
  createConsumerForLatest,
  fetchLatestOffset,
  fetchOffsets,
  sendMessage
}
